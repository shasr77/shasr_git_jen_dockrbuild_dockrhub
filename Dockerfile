FROM openjdk:8
EXPOSE 8080
ADD target/docker_dockerhub_mavenplugin.jar docker_dockerhub_mavenplugin.jar
ENTRYPOINT ["java", "-jar", "/docker_dockerhub_mavenplugin.jar"]