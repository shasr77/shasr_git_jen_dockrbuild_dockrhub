package com.srs.learning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class DockerDockerhubMavenpluginApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerDockerhubMavenpluginApplication.class, args);
	}

	@GetMapping
	public String getTextData() {
		return "Dockerize SpringBoot App and push to DockerHub with Maven plugin...!";
	}
}
